package main

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/aws/aws-lambda-go/lambda"
)

var BASE_URL string = "https://api.chatwork.com/v2"
var API_TOKEN string = "3d3639bd08b4155dd5e900ab07384b0c"
var ROOM_ID string = "93585931"

type MyEvent struct {
	Name string `json:"name"`
}

func HandleRequest(ctx context.Context, name MyEvent) (string, error) {
	apiResponse := notify("gitにPushされました")
	return fmt.Sprintf("Hello %s!2", name.Name+":"+apiResponse), nil
}

func notify(message string) string {
	api_url := BASE_URL + "/rooms/" + ROOM_ID + "/messages"
	values := url.Values{}
	values.Add("body", message)

	fmt.Println(api_url)
	req, err := http.NewRequest(
		"POST",
		api_url,
		strings.NewReader(values.Encode()),
	)
	if err != nil {
		fmt.Print(err)
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("X-ChatWorkToken", API_TOKEN)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		fmt.Print(err)
	}
	fmt.Println(resp.StatusCode)

	body, err := ioutil.ReadAll(resp.Body)
	buf := bytes.NewBuffer(body)
	json := buf.String()
	fmt.Println(json)

	defer resp.Body.Close()

	return json
}
func main() {
	lambda.Start(HandleRequest)
	// notify("Gitにpushされました")
}
